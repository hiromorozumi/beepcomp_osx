#!/bin/bash

#
#	create the necessary folder structure
#

if [ -d "./release" ];then
	rm -r ./release
fi

mkdir -p ./release
mkdir -p ./release/BeepComp.app
mkdir -p ./release/BeepComp.app/Contents
mkdir -p ./release/BeepComp.app/Contents/Resources
mkdir -p ./release/BeepComp.app/Contents/MacOS
mkdir -p ./release/BeepComp.app/Contents/MacOS/fonts
mkdir -p ./release/BeepComp.app/Contents/MacOS/images
mkdir -p ./release/BeepComp.app/Contents/MacOS/documentation
mkdir -p ./release/BeepComp.app/Contents/MacOS/userdata

#
#	create the "Info.plist" file
#

cp ./Info.plist ./release/BeepComp.app/Contents/.

#
#	create the icon set then copy them
#

ICONSIN=./images
ICONTEMP=beepcomp.iconset
ICONSOUT=./release/BeepComp.app/Contents/Resources

mkdir -p "$ICONTEMP"
sips -z 16 16	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_16x16.png
sips -z 32 32	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_16x16@2x.png
sips -z 32 32	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_32x32.png
sips -z 64 64	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_32x32@2x.png
sips -z 128 128	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_128x128.png
sips -z 256 256	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_128x128@2x.png
sips -z 256 256	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_256x256.png
sips -z 512 512	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_256x256@2x.png
sips -z 512 512	$ICONSIN/beepcomp_icon_1024.png --out $ICONTEMP/icon_512x512.png
cp $ICONSIN/beepcomp_icon_1024.png $ICONTEMP/icon_512x512@2x.png
iconutil --convert icns beepcomp.iconset
rm -R ./beepcomp.iconset
cp ./beepcomp.icns $ICONSOUT/.

#
#	copy all the dylibs that executable depends on
#

# DYLIBDEST=./release/BeepComp.app/Contents/MacOS/.

# cp /usr/local/lib/libsndfile.1.dylib $DYLIBDEST
# cp /usr/local/opt/lame/lib/libmp3lame.0.dylib $DYLIBDEST
# cp /usr/local/opt/portaudio/lib/libportaudio.2.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-graphics.2.4.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-window.2.4.dylib $DYLIBDEST
# cp /usr/local/lib/libsfml-system.2.4.dylib $DYLIBDEST
# cp /usr/lib/libc++.1.dylib $DYLIBDEST
# cp /usr/lib/libSystem.B.dylib $DYLIBDEST

#
#	update the dylib paths for the bundled executable to refer to
#

# install_name_tool -change /usr/local/lib/libsndfile.1.dylib @executable_path/libsndfile.1.dylib beepcomp
# install_name_tool -change /usr/local/opt/lame/lib/libmp3lame.0.dylib @executable_path/libmp3lame.0.dylib beepcomp
# install_name_tool -change /usr/local/opt/portaudio/lib/libportaudio.2.dylib @executable_path/libportaudio.2.dylib beepcomp
# install_name_tool -change @rpath/libsfml-graphics.2.4.dylib @executable_path/libsfml-graphics.2.4.dylib beepcomp
# install_name_tool -change @rpath/libsfml-window.2.4.dylib @executable_path/libsfml-window.2.4.dylib beepcomp
# install_name_tool -change @rpath/libsfml-system.2.4.dylib @executable_path/libsfml-system.2.4.dylib beepcomp
# install_name_tool -change /usr/lib/libc++.1.dylib @executable_path/libc++.1.dylib beepcomp
# install_name_tool -change /usr/lib/libSystem.B.dylib @executable_path/libSystem.B.dylib beepcomp

#
#	copy the executable into the Contents/MacOS folder
#

cp ./beepcomp ./release/BeepComp.app/Contents/MacOS/.

#
#	bundle all dependent dylibs using macdylibbundler
#

dylibbundler -od -b -x ./release/BeepComp.app/Contents/MacOS/beepcomp -d ./release/BeepComp.app/Contents/libs

#
#	copy all additional files that the executable uses into Contents/MacOS
#

cp ./fonts/* ./release/BeepComp.app/Contents/MacOS/fonts/
cp ./images/* ./release/BeepComp.app/Contents/MacOS/images/
cp ./userdata/*.txt ./release/BeepComp.app/Contents/MacOS/userdata/
cp ./documentation/* ./release/BeepComp.app/Contents/MacOS/documentation/
cp ./default.txt ./release/BeepComp.app/Contents/MacOS/
cp ./LICENSE.txt ./release/BeepComp.app/Contents/MacOS/
cp ./README.md ./release/BeepComp.app/Contents/MacOS/
cp ./README.txt ./release/BeepComp.app/Contents/MacOS/
echo "" >>./release/BeepComp.app/Contents/MacOS/beepcomp.config
chmod 777 ./release/BeepComp.app/Contents/MacOS/beepcomp.config

cd ./release
zip -r "BeepComp.zip" ./BeepComp.app
mv ./BeepComp.zip ./BeepComp_v0-2-2_mac.zip

echo "FINISHED! Now you have the app bundle and a zip file of the app bundle in the 'release' folder. Opening the 'release' folder now..."

open ./