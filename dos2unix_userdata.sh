#!/bin/bash

if [ -d "./userdata/userdata_unix" ]; then
	rm -r ./userdata/userdata_unix
fi

mkdir -p ./userdata/userdata_unix

cd userdata

for filename in *.txt; do
	tr -d '\015' < "$filename" > userdata_unix/"$filename"
done